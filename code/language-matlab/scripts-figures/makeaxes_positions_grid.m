function axes_positions = makeaxes_positions_grid(xoffset,yoffset,width,height,xsep,ysep,m,n)
%% axes_positions = makeaxes_positions_grid(xoffset,yoffset,width,height,xsep,ysep,m,n)
%% 
%% create well laid out set of m x n axis locations
%% 
%% axes_positions will be a transposed matrix of positions so that
%% indices to cell positions will run across rows, from top to
%% bottom 
%% 
%% so: to plot column first, take the use of axes_positions
%% 
%% see also makeaxes_inset_position

k=0;
for i=1:m
    for j=1:n
        k=k+1;
        axes_positions(j,i).box = [xoffset + (j-1)*(width + xsep),...
                            yoffset + (m-i)*(height + ysep),...
                            width,height];
    end
end
