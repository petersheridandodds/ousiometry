clear general_settings;

load ../analysis/character_data.mat;

%% some of what we should have:
%% 
%% A: Ntraits x Ncharacters matrix
%%    Mid score of 50 removed uniformly
%%    Rescaled so all values fall in -1/2 to 1/2
%% 
%% characters_table
%% e.g.,
%% ID     FictionalWork    CharacterDisplayName 
%% {'GP/1'}    {'The Good Place'}    {'Eleanor Shellstrop'}
%% 
%% bap_table: Bipolar adjective pairs for semantic differentials
%% e.g.,
%% ID    low_leftAnchor  high_rightAnchor     
%% {'BAP203'}    {'cool'}    {'dorky'}
%% 
%% SVD matrices: U, Sigma, V, 
%% sigma values: sigmas
%% 
%% >> whos
%% 
%%  Name                    Size               Bytes  Class     Attributes
%% 
%%   Sigma                 235x800            1504000  double              
%%   U                     235x235             441800  double              
%%   V                     800x800            5120000  double              
%%   bap_table             235x3                85437  table               
%%   characters_table      800x3               301319  table               
%%   sigmas                235x1                 1880  double           
%% 

Ntypes(1) = size(A,1);
Ntypes(2) = size(A,2);

typenames1 = bap_typenames;
typenames2 = character_story_abbrev_typenames;

%% U or V
%% e.g., if 1 = traits, 2 = characters
%% then
%% if maindim = 1, eigentraits will be described by characters
%% if maindim = 2, eigencharacters will be described by traits

%% eigentraits;
%% primary_dim = 1;
%% eigencharacters:
primary_dim = 2;

second_dim = 3 - primary_dim;

%% two dimensions for ousiogram, from U or V
%% choice of eigentraits or eigencharacters
dim1 = 1;
dim2 = 3;



%% lexicon style plot, no popularity
token_frequencies = ones(Ntypes(primary_dim),1);

%% 
if (primary_dim == 1)
    type_dimension_weights = A * V(:,[dim1 dim2]);
    coordnames{1} = sprintf('\\hat{u}_{%d}',dim1);
    coordnames{2} = sprintf('\\hat{u}_{%d}',dim2);
    typenames = typenames1;
else
    type_dimension_weights = transpose(U(:,[dim1 dim2])' * A);
    %%    coordnames{1} = sprintf('\\hat{v}_{%d}',dim1);
    %%    coordnames{2} = sprintf('\\hat{v}_{%d}',dim2);
    coordnames{1} = sprintf('P');
    coordnames{2} = sprintf('S');
    typenames = typenames2;
end



i=0;
i=i+1;
dimension_settings(i).name = 'Power';
dimension_settings(i).namelc = 'power';
dimension_settings(i).notation = coordnames{1};
dimension_settings(i).range = [-1 1];
dimension_settings(i).positive = 'more of a hero';
dimension_settings(i).negative = 'more of a zero';
dimension_settings(i).positive1word = 'powerful';
dimension_settings(i).negative1word = 'weak';
i=i+1;
dimension_settings(i).name = 'Structure';
dimension_settings(i).namelc = 'structure';
dimension_settings(i).notation = coordnames{2};
dimension_settings(i).range = [-1 1];
dimension_settings(i).positive = 'more playful';
dimension_settings(i).negative = 'more serious';
dimension_settings(i).positive1word = 'playful';
dimension_settings(i).negative1word = 'serious';




%% coordinate system

general_settings.colorbarpos = [-.975 +0.60];

general_settings.title = sprintf('%s-%s space across the multi-storyverse',...
                                 dimension_settings(1).name, ...
                                 dimension_settings(2).name);

general_settings.coordnames = coordnames;

general_settings.filetag = sprintf('characters_003');

general_settings.showellipse = 'no';

general_settings.overallmaxtoken_frequencies = 600;
general_settings.colorbartoken_frequencies = [1, 25:25:250];
%% general_settings.colormaptype = 'normalized';


%% no underlying annotations
%% general_settings.do_not_annotate = words;




%% 
theta = -45*pi/180;

%% rotate by pi/4
Rot2d = [
      cos(theta) -sin(theta);
      sin(theta) cos(theta);
    ];

%% general_settings.do_annotate.altvalues = [pleasurescores, arousalscores] * Rot2d';


i=0;
i=i+1;
general_settings.do_annotate.categories{i} = 'playful';
i=i+1;
general_settings.do_annotate.categories{i} = 'playful-powerful';
i=i+1;
general_settings.do_annotate.categories{i} = 'powerful';
i=i+1;
general_settings.do_annotate.categories{i} = 'serious-powerful';
i=i+1;
general_settings.do_annotate.categories{i} = 'serious';
i=i+1;
general_settings.do_annotate.categories{i} = 'serious-weak';
i=i+1;
general_settings.do_annotate.categories{i} = 'weak';
i=i+1;
general_settings.do_annotate.categories{i} = 'playful-weak';




%% general_settings.horizontal_axis_label = sprintf('%s', ...
%%                 'displeasure-pleasure');

%% general_settings.vertical_axis_label = sprintf('%s', ...
%%                 'sleep-arousal');


general_settings.annotate_internally = 'no';
%% general_settings.phivals = [0:45:315]*pi/180;


general_settings.Nannotations_horizontal = 5;
general_settings.Nannotations_vertical = 5;

general_settings.imageformat.open = 'yes';

%% set token_frequencies to ones for lexicon
%% set token_frequencies to zipf distribution frequencies for a corpus

things = figousiometer7300(typenames, ...
                  token_frequencies, ...
                  type_dimension_weights, ...
                  dimension_settings, ...
                  general_settings);

maxcolorindices = things.maxcolorindex_data;

