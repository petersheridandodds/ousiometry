clear settings;

load ousiometry_data words indexpairs;

Nwords = length(words);

counts = ones(Nwords,1);

%% coordinate system
%% VAD, GES, PDS (default)
coords = {'VAD','GES','PDS'};

settings.colorbarpos = [-.975 +0.60];

settings.titleinsert = 'the NRC VAD lexicon';

for j=1:length(coords);
    settings.coords = coords{j};

    for indexpair_i = 1:length(indexpairs)

        settings.filetag = sprintf('%s001_%d',coords{j},indexpair_i);

        indexpair = indexpairs{indexpair_i};
        settings.indexpair = indexpair;
        settings.indexpair_i = indexpair_i;

        settings.showellipse = 'yes';
        
        settings.overallmaxcounts = 600;
        settings.colorbarcounts = [1, 25:25:250];
        %% settings.colormaptype = 'normalized';
        
        settings.do_not_annotate = {'masturbation'};
        
        settings.Nannotations_horizontal = 5;
        settings.Nannotations_vertical = 5;

        settings.imageformat.open = 'no';

        %% set counts to ones for lexicon
        %% set counts to zipf distribution frequencies for a corpus
        things = figousiometer9000(counts,settings);
        
        maxcolorindices(indexpair_i,j) = things.maxcolorindex_data;
    end
end

!combinepdfs figousiometer9000/figousiometer9000_VAD001_?_noname.pdf figousiometer9000/figousiometer9000_GES001_?_noname.pdf figousiometer9000/figousiometer9000_PDS001_?_noname.pdf figousiometer9000/figousiometer9000_VAD_GES_PDS001_combined.pdf

!open figousiometer9000/figousiometer9000_VAD_GES_PDS001_combined.pdf
