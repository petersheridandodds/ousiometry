%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% color bar
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (isfield(settings,'colorbarpos'))
    colorbarpos = settings.colorbarpos;
else
    colorbarpos = [-.975 +0.65];
end



if (isfield(settings,'colorbarcounts'))
    colorbarcounts = settings.colorbarcounts;
    Nboxes = length(colorbarcounts);
else
    if (isfield(settings,'Nboxes'))
        Nboxes = settings.Nboxes;
    else
        Nboxes = 10;
    end
    colorbarcounts = floor(linspace(1,overallmaxcounts,Nboxes));
end


if (~isfield(settings,'colormaptype'))
    settings.colormaptype = 'normalized';
end

if (strcmp(settings.colormaptype,'indexed'))

    for i=1:Nboxes
        tmppos = [colorbarpos(1), ...
                  colorbarpos(2) + (i-1)*binsize, ...
                  binsize, ...
                  binsize];
        tmph = rectangle('position',tmppos);
        hold on;
        
        colorindex = colorbarcounts(i);
        set(tmph,'facecolor',heatmapcolors(colorindex,:));
        set(tmph,'edgecolor',0.7*heatmapcolors(colorindex,:));
        
        tmpXcoord = colorbarpos(1) + 1.2*binsize;
        tmpYcoord = colorbarpos(2) + 0.5*binsize + (i-1)*binsize;
        
        tmpstr = sprintf('%d',colorindex);

        tmpcolor = colors.verydarkgrey;
        tmph = text(tmpXcoord,tmpYcoord,tmpstr, ...
                    'fontsize',12, ...
                    'units','data', ...
                    'color',tmpcolor, ...
                    'horizontalalignment','left', ...
                    'verticalalignment','middle', ...
                    'interpreter','latex');

    end

else %% normalized

    
    for i=1:Nboxes
        tmppos = [colorbarpos(1), ...
                  colorbarpos(2) + (i-1)*binsize, ...
                  binsize, ...
                  binsize];
        tmph = rectangle('position',tmppos);
        hold on;
        
        colorindex = colorbarcounts(i);

        set(tmph,'facecolor',heatmapcolors(colorindex,:));
        set(tmph,'edgecolor',0.7*heatmapcolors(colorindex,:));
        
        tmpXcoord = colorbarpos(1) + 1.2*binsize;
        tmpYcoord = colorbarpos(2) + 0.5*binsize + (i-1)*binsize;
        
        if (i==1)
            tmpstr = '$1$';
            tmpcolor = colors.verydarkgrey;
            tmph = text(tmpXcoord,tmpYcoord,tmpstr, ...
                        'fontsize',12, ...
                        'units','data', ...
                        'color',tmpcolor, ...
                        'horizontalalignment','left', ...
                        'verticalalignment','middle', ...
                        'interpreter','latex');
        elseif (i==Nboxes)
            tmpstr = sprintf('%s',addcommas(max(binned_zipfcounts(:))));
            tmpcolor = colors.verydarkgrey;
            tmph = text(tmpXcoord,tmpYcoord,tmpstr, ...
                        'fontsize',12, ...
                        'units','data', ...
                        'color',tmpcolor, ...
                        'horizontalalignment','left', ...
                        'verticalalignment','middle', ...
                        'interpreter','latex');
        end

    end
    
end

