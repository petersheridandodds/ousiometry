notations.valence = 'V';
notations.arousal = 'A';
notations.dominance = 'D';

notations.goodness = 'G';
notations.energy = 'E';

notations.power = 'P';
notations.danger = 'D';

notations.structure = 'S';


notations.chaos = 'C';
notations.playfulness = 'P';
notations.structure = 'S';


coords = {'VAD','GES','PDS'};

mapcoords = containers.Map(coords,[1 2 3]);

j=0;
j=j+1;
i=0;
i=i+1;
dimensions(i,j).name = 'Valence';
dimensions(i,j).namelc = 'valence';
dimensions(i,j).notation = notations.valence;
dimensions(i,j).range = [-.5 .5];
dimensions(i,j).positive = 'more positive';
dimensions(i,j).negative = 'more negative';
dimensions(i,j).positive1word = 'positive';
dimensions(i,j).negative1word = 'negative';
i=i+1;
dimensions(i,j).name = 'Arousal';
dimensions(i,j).namelc = 'arousal';
dimensions(i,j).notation = notations.arousal;
dimensions(i,j).range = [-.5 .5];
dimensions(i,j).positive = 'more active';
dimensions(i,j).negative = 'more passive';
dimensions(i,j).positive1word = 'active';
dimensions(i,j).negative1word = 'passive';
i=i+1;
dimensions(i,j).name = 'Dominance';
dimensions(i,j).namelc = 'dominance';
dimensions(i,j).notation = notations.dominance;
dimensions(i,j).range = [-.5 .5];
dimensions(i,j).positive = 'more dominant';
dimensions(i,j).negative = 'more submissive';
dimensions(i,j).positive1word = 'dominant';
dimensions(i,j).negative1word = 'submissive';


j=j+1;
i=0;
i=i+1;
dimensions(i,j).name = 'Goodness';
dimensions(i,j).namelc = 'goodness';
dimensions(i,j).notation = notations.goodness;
dimensions(i,j).range = [-1 1];
dimensions(i,j).positive = 'better';
dimensions(i,j).negative = 'worse';
dimensions(i,j).positive1word = 'good';
dimensions(i,j).negative1word = 'bad';
i=i+1;
dimensions(i,j).name = 'Energy';
dimensions(i,j).namelc = 'energy';
dimensions(i,j).notation = notations.energy;
dimensions(i,j).range = [-1 1];
dimensions(i,j).positive = 'more energy';
dimensions(i,j).negative = 'less energy';
dimensions(i,j).positive1word = 'high energy';
dimensions(i,j).negative1word = 'low energy';
i=i+1;
dimensions(i,j).name = 'Structure';
dimensions(i,j).namelc = 'structure';
dimensions(i,j).notation = notations.structure;
dimensions(i,j).range = [-1 1];
dimensions(i,j).positive = 'less structured';
dimensions(i,j).negative = 'more structured';
dimensions(i,j).positive1word = 'unstructured';
dimensions(i,j).negative1word = 'structured';

j=j+1;
i=0;
i=i+1;
dimensions(i,j).name = 'Power';
dimensions(i,j).namelc = 'power';
dimensions(i,j).notation = notations.power;
dimensions(i,j).range = [-1 1];
dimensions(i,j).positive = 'more powerful';
dimensions(i,j).negative = 'weaker';
dimensions(i,j).positive1word = 'powerful';
dimensions(i,j).negative1word = 'weak';
i=i+1;
dimensions(i,j).name = 'Danger';
dimensions(i,j).namelc = 'danger';
dimensions(i,j).notation = notations.danger;
dimensions(i,j).range = [-1 1];
dimensions(i,j).positive = 'more dangerous';
dimensions(i,j).negative = 'safer';
%% dimensions(i,j).negative = 'less dangerous';
dimensions(i,j).positive1word = 'dangerous';
dimensions(i,j).negative1word = 'safe';
%% dimensions(i,j).negative1word = 'nondangerous';
i=i+1;
dimensions(i,j).name = 'Structure';
dimensions(i,j).namelc = 'structure';
dimensions(i,j).notation = notations.structure;
dimensions(i,j).range = [-1 1];
dimensions(i,j).positive = 'less structured';
dimensions(i,j).negative = 'more structured';
dimensions(i,j).positive1word = 'unstructured';
dimensions(i,j).negative1word = 'structured';

