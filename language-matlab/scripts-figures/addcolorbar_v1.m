%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% color bar
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

colorbarcounts = [1, 25:25:225]';

for i=1:length(colorbarcounts);
    tmppos = [tmpcolorbarpos(1), ...
              tmpcolorbarpos(2) + (i-1)*binsize, ...
              binsize, ...
              binsize];
    tmph = rectangle('position',tmppos);
    hold on;
            
    colorindex = colorbarcounts(i);
    set(tmph,'facecolor',heatmapcolors(colorindex,:));
    set(tmph,'edgecolor',0.7*heatmapcolors(colorindex,:));
    
    tmpXcoord = tmpcolorbarpos(1) + 1.2*binsize;
    tmpYcoord = tmpcolorbarpos(2) + 0.5*binsize + (i-1)*binsize;
    
    tmpstr = sprintf('%d',colorindex);

    tmpcolor = colors.verydarkgrey;
    tmph = text(tmpXcoord,tmpYcoord,tmpstr, ...
                'fontsize',12, ...
                'units','data', ...
                'color',tmpcolor, ...
                'horizontalalignment','left', ...
                'verticalalignment','middle', ...
                'interpreter','latex');


end
