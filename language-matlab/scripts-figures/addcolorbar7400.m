%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% color bar
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

binsize_colorbar = 0.1/3;


if (isfield(general_settings,'colorbarpos'))
    colorbarpos = general_settings.colorbarpos;
else
    colorbarpos = [-.975 +0.65];
end



if (isfield(general_settings,'colorbarcounts'))
    colorbarcounts = general_settings.colorbarcounts;
    Nboxes = length(colorbarcounts);
else
    if (isfield(general_settings,'Nboxes'))
        Nboxes = general_settings.Nboxes;
    else
        Nboxes = 10;
    end
    colorbarcounts = floor(linspace(1,overallmaxcounts,Nboxes));
end


if (~isfield(general_settings,'colormaptype'))
    general_settings.colormaptype = 'normalized';
end

if (strcmp(general_settings.colormaptype,'indexed'))

    for i=1:Nboxes
        tmppos = [colorbarpos(1), ...
                  colorbarpos(2) + (i-1)*binsize_colorbar, ...
                  binsize_colorbar, ...
                  binsize_colorbar];
        tmph = rectangle('position',tmppos);
        hold on;
        
        colorindex = colorbarcounts(i);
        set(tmph,'facecolor',heatmapcolors(colorindex,:));
        set(tmph,'edgecolor',0.7*heatmapcolors(colorindex,:));
        
        tmpXcoord = colorbarpos(1) + 1.2*binsize_colorbar;
        tmpYcoord = colorbarpos(2) + 0.5*binsize_colorbar + (i-1)*binsize_colorbar;
        
        tmpstr = sprintf('%d',colorindex);

        tmpcolor = colors.verydarkgrey;
        tmph = text(tmpXcoord,tmpYcoord,tmpstr, ...
                    'fontsize',12, ...
                    'units','data', ...
                    'color',tmpcolor, ...
                    'horizontalalignment','left', ...
                    'verticalalignment','middle', ...
                    'interpreter','latex');

    end

else %% normalized

    
    for i=1:Nboxes
        tmppos = [colorbarpos(1), ...
                  colorbarpos(2) + (i-1)*binsize_colorbar, ...
                  binsize_colorbar, ...
                  binsize_colorbar];
        tmph = rectangle('position',tmppos);
        hold on;
        
        colorindex = colorbarcounts(i);

        set(tmph,'facecolor',heatmapcolors(colorindex,:));
        set(tmph,'edgecolor',0.7*heatmapcolors(colorindex,:));
        
        tmpXcoord = colorbarpos(1) + 1.2*binsize_colorbar;
        tmpYcoord = colorbarpos(2) + 0.5*binsize_colorbar + (i-1)*binsize_colorbar;
        
        if (i==1)
            tmpstr = '$1$';
            tmpcolor = colors.verydarkgrey;
            tmph = text(tmpXcoord,tmpYcoord,tmpstr, ...
                        'fontsize',12, ...
                        'units','data', ...
                        'color',tmpcolor, ...
                        'horizontalalignment','left', ...
                        'verticalalignment','middle', ...
                        'interpreter','latex');
        elseif (i==Nboxes)
            tmpstr = sprintf('%s',addcommas(max(binnedcounts(:))));
            tmpcolor = colors.verydarkgrey;
            tmph = text(tmpXcoord,tmpYcoord,tmpstr, ...
                        'fontsize',12, ...
                        'units','data', ...
                        'color',tmpcolor, ...
                        'horizontalalignment','left', ...
                        'verticalalignment','middle', ...
                        'interpreter','latex');
        end

    end
    
end

