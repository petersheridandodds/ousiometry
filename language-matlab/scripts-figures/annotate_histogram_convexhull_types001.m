function annotatedtypeslist = annotate_histogram_convexhull_types001(xvals,yvals,types,annotation_settings)

%% 
%% annotatedtypeslist = annotate_histogram_convexhull_types001(xvals,yvals,types,annotation_settings)
%% 
%% full help section not done yet
%% 
%% ::::::::::::::::::::
%% annotation_settings
%% 
%% some of the many options:
%% 
%% - Allow for alternate display of types when angle of display is
%% flipped going down around the left side of the ousiogram
%% 
%% include:
%% 
%% annotation_settings.types_alt
%% 
%% which must have be a cell array off strings and have the same
%% dimensions as types
%% 
%% example: good-bad versus bad-good



%% settings


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(isfield(annotation_settings,'fontsize'))
    annotation_fontsize = annotation_settings.fontsize;
else
    annotation_fontsize = 18;
end
    

channelwidth = 0.65; %% half width of channel to search in
linefactor = 4; %% now long annotation can be relatively

channelwidth = 0.65; %% half width of channel to search in
linefactor = 4; %% now long annotation can be relatively


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% remove types if requested (identified during figure making
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (length(annotation_settings.excludedtypes) > 0)
    for i=1:length(annotation_settings.excludedtypes)
        indices = find(~strcmp(types,...
                               annotation_settings.excludedtypes{i}));
        types = types(indices);
        xvals = xvals(indices);
        yvals = yvals(indices);
    end
end

annotatedtypeslist = {};
annotatedtypes = zeros(size(types));

if (length(types)>0)

    loadcolors;

    if (length(xvals) <= 2) %% add box
        
        if (length(xvals) == 2)
            if (yvals(1) > yvals(2))
                yvals(1) = yvals(1) + annotation_settings.binwidth;
                yvals(2) = yvals(2) - annotation_settings.binwidth;
            else
                yvals(1) = yvals(1) - annotation_settings.binwidth;
                yvals(2) = yvals(2) + annotation_settings.binwidth;
            end
        else
            yvals(1) = yvals(1) + annotation_settings.binwidth;
        end
        
        for i=1:length(xvals)
            tmpXcoord = xvals(i);
            tmpYcoord = yvals(i);
            clear tmpstr;
            tmpstr = sprintf(' %s ',types{i});

            annotatedtypeslist{end+1} = types{i};
            
            if (mod(i,2)==1)
                tmpcolor = colors.darkgrey;
            else
                tmpcolor = colors.verydarkgrey;
            end
            
            tmph = text(tmpXcoord,tmpYcoord,tmpstr, ...
                        'fontsize',18, ...
                        'units','data', ...
                        'color',tmpcolor, ...
                        'rotation',0,...
                        'horizontalalignment','center', ...
                        'verticalalignment','middle', ...
                        'interpreter','latex');
        end
    end
    
    
    if (length(xvals) > 2)
        %% counter-clockwise
        convexhull_indices = convhull(xvals,yvals);

        convexhull_xvals = col(xvals(convexhull_indices));
        convexhull_yvals = col(yvals(convexhull_indices));

        dists = cumsum([0; sqrt(col((diff(convexhull_xvals).^2 + ...
                                     diff(convexhull_yvals).^2)))]);    

        
        Nannotations = round(dists(end)/annotation_settings.binwidth);
        distsmod = col(linspace(0,dists(end),Nannotations+1));
        even_coords = interp1(...
            dists, ...
            [convexhull_xvals, convexhull_yvals], ...
            distsmod);
        


        %% find normals
        normals = diff([even_coords(:,2),-even_coords(:,1)]);
        unitnormals = normals./((sum(normals.^2,2)).^.5*[1 1]);
        
        %% angles for words
        normal_angles = atan2(unitnormals(:,2),unitnormals(:,1));

        %% mid points
        even_coords_midpoints = even_coords(1:end-1,:) + 0.5*diff(even_coords);
        
        %% find closest type 
        %% overall, projected, and perpendicular distance

        annotation_dists = zeros(Nannotations,length(xvals));
        annotation_dists_proj = zeros(Nannotations,length(xvals));
        annotation_dists_perp = zeros(Nannotations,length(xvals));

        %% overall distance
        for i=1:Nannotations
            annotation_dists(i,1:length(xvals)) = ...
                ((row(xvals) - even_coords_midpoints(i,1)).^2 ...
                 + ...
                 (row(yvals) - even_coords_midpoints(i,2)).^2).^.5;
        end
        
        %% line:
        %% even_coords_midpoints' + t * unitnormals'
        %% points:
        %% [xvals yvals]';
        
        %% projected distance
        for i=1:Nannotations
            annotation_dists_proj(i,1:length(xvals)) = ...
                unitnormals(i,:) * ( even_coords_midpoints(i,:)' - [xvals ...
                                yvals]');
        end

        %% perpendicular distance
        for i=1:Nannotations
            for j=1:length(xvals)
                tmpvec = (even_coords_midpoints(i,:)' - [xvals(j) ...
                                    yvals(j)]') ...
                         - ...
                         ((even_coords_midpoints(i,:) - [xvals(j) yvals(j)]) * ...
                          unitnormals(i,:)') * unitnormals(i,:)';
                annotation_dists_perp(i,j) = sqrt(sum(tmpvec.*tmpvec));
            end
        end    
        
        
        
        %%    [~,overallindices] = sort(normalprojections(:),'descend');
        %%    [indices_row, indices_col] = ind2sub(size(normalprojections),overallindices);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% find types closest to midpoints (within 0.5-ish * binwidth on
        %% either side)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        %% excluded words outside of channel defined by normal
        indices = find(abs(annotation_dists_perp) > channelwidth*annotation_settings.binwidth);
        annotation_dists_proj_allowed = annotation_dists_proj;
        annotation_dists_proj_allowed(indices) = +Inf;

        %%    [normalmax,typeindices] = min(annotation_dists,[],2);

        for i=1:Nannotations
            [minval,index] = min(annotation_dists_proj_allowed(i,:));
            if (minval < +Inf) 
                typeindices(i) = index;
                %% prevent types repeating:
                annotation_dists_proj_allowed(:,index) = +Inf;
            else
                typeindices(i) = 0;
            end
            minvals(i) = minval;
        end


        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% add words in normal directions
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        %% convex hull
        %%        tmph = plot(convexhull_xvals,convexhull_yvals,'-');
        %%        set(tmph,'color',colors.blue);
        %%        hold on;
        
        %% convex hull
        %%        tmph = plot(even_coords([1:end],1),even_coords([1:end],2),'-');
        %%        set(tmph,'color',colors.red);
        %%        hold on;

        
        %% normals
        %%     tmph = plot(even_coords(1,1),even_coords(1,2),'ko');
        %%         set(tmph,'color',colors.red);
        %%         quiver(even_coords_midpoints(:,1),even_coords_midpoints(:,2), ...
        %%                3*unitnormals(:,1),3*unitnormals(:,2));
        %    axis equal;

        typeindices;
        xvals;
        yvals;
        
        %%    [~,indices] = sort(minvals(typindices),
        
        for i=1:length(typeindices)

            if (typeindices(i) > 0)
                tmpXcoord = even_coords_midpoints(i,1) + annotation_settings.textoffset*unitnormals(i,1);
                tmpYcoord = even_coords_midpoints(i,2) + annotation_settings.textoffset*unitnormals(i,2);

                tmpXcoord1 = xvals(typeindices(i));
                tmpYcoord1 = yvals(typeindices(i));
                
                linelength = sqrt((tmpXcoord-tmpXcoord1)^2 + ...
                                  (tmpYcoord-tmpYcoord1)^2);
                
                if (linelength < linefactor*annotation_settings.binwidth)
                    
                    annotation_xvals = linspace(tmpXcoord,tmpXcoord1,100);
                    annotation_yvals = linspace(tmpYcoord,tmpYcoord1,100);
                    tmph = plot(annotation_xvals,annotation_yvals,'k-');
                    set(tmph,'color',colors.darkgrey);
                    
                    clear tmpstr;

                    tmpstr = sprintf(' %s ',types{typeindices(i)});
                    %% adjust leading quotes for latex:
                    tmpstr = regexprep(tmpstr,' ''',' `');
                    tmpstr = regexprep(tmpstr,'^''','`');
                    annotatedtypeslist{end+1} = types{typeindices(i)};

                    if (abs(normal_angles(i)) < pi/2)
                        tmpangle = 180/pi*normal_angles(i);
                        tmphalign = 'left';
                        
                        if (isfield(annotation_settings, ...
                                    'types_alt'))
                            if (tmpXcoord < 0)
                                %%                                tmpstr = sprintf(' %s ',annotation_settings.types_alt{typeindices(i)})
                                %% adjust leading quotes for latex:
                                tmpstr = regexprep(tmpstr,' ''',' `');
                                tmpstr = regexprep(tmpstr,'^''','`');
                            end
                        end

                    else 
                        tmpangle = 180/pi*(normal_angles(i) + pi);
                        tmphalign = 'right';
                        %% adjust to alternate if exists
                        if (isfield(annotation_settings,'types_alt'))
                            tmpstr = sprintf(' %s ',annotation_settings.types_alt{typeindices(i)});
                            %% adjust leading quotes for latex:
                            tmpstr = regexprep(tmpstr,' ''',' `');
                            tmpstr = regexprep(tmpstr,'^''','`');
                        end
                    end
                    

                    
                    if (mod(i,2)==1)
                        tmpcolor = colors.darkgrey;
                    else
                        tmpcolor = colors.verydarkgrey;
                    end

                    
                    tmpstr;
                    tmph = text(tmpXcoord,tmpYcoord,tmpstr, ...
                                'fontsize',18, ...
                                'units','data', ...
                                'color',tmpcolor, ...
                                'rotation',tmpangle,...
                                'horizontalalignment',tmphalign, ...
                                'verticalalignment','middle', ...
                                'interpreter','latex');
                end
            end
        end
    end
end

